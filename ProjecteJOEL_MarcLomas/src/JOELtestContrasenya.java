import static org.junit.Assert.*;
import java.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class JOELtestContrasenya {

    private String nom;
    private boolean funciona;

    public JOELtestContrasenya(String nom, boolean funciona) {
	this.nom = nom;
	this.funciona = funciona;
    }

    @Parameters
    public static Iterable<Object[]> nomUsuari() {
	return Arrays.asList(new Object[][] { { "Marc_122", true}, { "#1Holaa", true}, { "#marc_122", false}, 
	    		{ "Marc781", false}, { "Marc_", false}, { "hola", false}, { "/2", false}, { "1598", false} });

    }
    

    @Test

    public void testContra() {
	boolean correcte = programa.comprovarContra(nom);
	assertEquals(funciona, correcte);
//		fail("Not yet implemented");
    }

}
