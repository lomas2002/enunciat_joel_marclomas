import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ JOELtestContrasenya.class, JOELtestEspais.class, JOELtestPreguntarContra.class })
public class AllTests {

}
