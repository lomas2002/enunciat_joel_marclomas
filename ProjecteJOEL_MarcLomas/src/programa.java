import java.util.ArrayList;
import java.util.Scanner;

/**
 * <h2>Excercici per guardar usuari i contrasenya, i tamb� per poder
 * consultar-los.</h2>
 * 
 * @see <a href="http://www.google.com">Google</a>
 * @version 05-06-2021
 * @author Marc Lomas Bono
 *
 */
public class programa {
    static Scanner lector = new Scanner(System.in);
    /**
     * Arraylist per guardar tots els usuaris i contrasenyes validadades.
     */
    static ArrayList usuari = new ArrayList();

    public static void main(String[] args) {
	// TODO Auto-generated method stub
	/**
	 * n�mero de casos de prova.
	 */
	int casos = 0;
	casos = lector.nextInt();
	lector.nextLine();
	/**
	 * nom de l'usuari.
	 */
	String NomUsuari = "";
	/**
	 * Contrasenya de l'usuari.
	 */
	String contrasenya = "";
	solucio(casos, NomUsuari, contrasenya);
    }

    /**
     * Primer hem de posar la quantitat de casos de prova, i despr�s per cada cas de
     * prova hem d'introduir el nom d'usuari i la contrasenya d'aquest. El programa
     * respondr� depenent si has introdu�t b� les dades o no, i tamb� si has
     * preguntat la contrasenya d'un usuari i l'has posat correcte te la mostrar�.
     * 
     * @param cases �s el n�mero de casos de prova
     * @param nom �s el nom de l'usuari
     * @param pass �s la contrasenya de l'usuari
     */
    public static void solucio(int cases, String nom, String pass) {
	for (int i = 0; i < cases; i++) {
	    nom = lector.nextLine();
	    if (nom.charAt(0) == '?') {
		String nom2 = "";
		/**
		 * nom sense el interrogant
		 */
		nom2 = nom.substring(1);
		preguntaContra(nom2);
		
	    } else {
		pass = lector.nextLine();
		boolean nomCorrecte = comprovarEspais(nom);
		boolean continua = true;
		if (nomCorrecte == false) {
		    System.out.println("El nom NO ha de tenir espais.");
		    continua = false;
		} else if (nom.length() < 8) {
		    System.out.println("El nom ha de tenir 8 o m�s car�cters.");
		    continua = false;
		}
		if(continua == true) {
		    
			boolean contraEspais = comprovarEspais(pass);
			boolean contraCorrecte = comprovarContra(pass);
			if (contraEspais == false) {
			    System.out.println("La contrasenya NO ha de tenir espais.");
			} else if (contraCorrecte == false) {
			    System.out.println(
				    "La contrasenya ha de tenir m�nim una maj�scula,un n�mero i un s�mbol (! _ - $ % # / ( ) & > <).");
			} else {
			    System.out.println("Nom d�usuari i contrasenya guardats.");
			    usuari.add(nom);
			    usuari.add(pass);
			    // System.out.println(usuari);
			} 
		}
	    }
	}
    }

    /**
     * Comprova que el nom o la contrasenya no tinguin espais.
     * 
     * @param paraula �s el nom o la contrasenya a comprovar.
     * @return true si no hi ha espais i false si s� que en troba.
     */
    public static boolean comprovarEspais(String paraula) {
	int espais = 0;
	for (int i = 0; i < paraula.length(); i++) {
	    if (paraula.charAt(i) == ' ') {
		espais++;
	    }
	}

	if (espais > 0) {
	    return false;
	} else {
	    return true;
	}
    }

    /**
     * Comprova que la contrasenya t� la maj�scula, el n�mero i el s�mbol
     * necessaris.
     * 
     * @param contra �s la contrasenya de l'usuari
     * @return true si est� correcte i false si falta algun n�mero, alguna maj�scula o algun s�mbol
     */
    public static boolean comprovarContra(String contra) {
	int contNums = 0;
	int contMaj = 0;
	int contSim = 0;
	for (int i = 0; i < contra.length(); i++) {
	    if (Character.isUpperCase(contra.charAt(i))) {
		contMaj++;
	    } else if (Character.isDigit(contra.charAt(i))) {
		contNums++;
	    } else if (contra.charAt(i) == '!' || contra.charAt(i) == '_' || contra.charAt(i) == '-'
		    || contra.charAt(i) == '$' || contra.charAt(i) == '%' || contra.charAt(i) == '#'
		    || contra.charAt(i) == '/' || contra.charAt(i) == '(' || contra.charAt(i) == ')'
		    || contra.charAt(i) == '<' || contra.charAt(i) == '>' || contra.charAt(i) == '&') {
		contSim++;
	    }
	}

	if (contMaj == 0) {
	    return false;
	} else if (contNums == 0) {
	    return false;
	} else if (contSim == 0) {
	    return false;
	} else {
	    return true;
	}
    }

    /**
     * Compara el nom d'usuari que es vol utilitzar amb els que hi ha guardats, si
     * existeix et diu la contrasenya d'aquest, i si no existeix et surt el missatge
     * d'error.
     * 
     * @param nomUsuari �s el nom d'usuari sense el interrogant al principi.
     */
    public static void preguntaContra(String nomUsuari) {
	// System.out.println(nomUsuari);
	int num = usuari.indexOf(nomUsuari);
	/**
	 * El (num % 2 != 0) �s perqu� les contrasenyes es guarden en n�meros imparells, ja
	 * que els noms es guarden a partir del 0 (0, 2, 4...).
	 */
	if (num == -1 || num % 2 != 0) {
	    System.out.println("Aquest nom d'usuari no existeix.");
	} else {
	    System.out.println(usuari.get(num + 1));
	}

    }

}
